cid=$(</sys/block/mmcblk0/device/cid)

apt-get purge -y salt-common
apt-get -y autoremove
apt-get update
apt-get install -y salt-minion
rm /etc/salt/minion
echo "id: $cid
master: saltmaster0.3vee.net
master_finger: fe:fd:c6:f2:48:52:be:ef:f6:11:4d:64:1e:7f:be:2f" > /etc/salt/minion
reboot
